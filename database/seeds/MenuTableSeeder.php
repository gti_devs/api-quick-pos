<?php

use Illuminate\Database\Seeder;
use App\Menu;
use Faker\Provider\Image;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        Menu::truncate();

        $faker = \Faker\Factory::create();

        // And now, let's create a few articles in our database:
        for ($i = 1; $i < 50; $i++) {
            Menu::create([
                'nama_menu' => "Menu" . $i,
                'harga' => $i . "00000",
                'photo' => "",
            ]);
        }
    }
}
