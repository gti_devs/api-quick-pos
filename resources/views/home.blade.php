@extends('layouts.app')

@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <!-- <li class="breadcrumb-item"><a href="#">Menu</a></li> -->
        <li class="breadcrumb-item active" aria-current="page">Menu</li>
      </ol>
    </nav>
    <div class="card bg-light mb-3">
      <div class="card-header float-left">
          <h4 class="float-left">Menu <small> - List Menu</small> </h4>
          <button type="button" class="btn btn-primary float-right">Create</button>
      </div>
      <div class="card-body">
        <table class="table table-hover">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">First</th>
                <th scope="col">Last</th>
                <th scope="col">Handle</th>
                <th style="width:180px">Action</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope="row">1</th>
                <td>Mark</td>
                <td>Otto</td>
                <td>@mdo</td>
                <td>
                    <a class="btn btn-warning" href="" data-toltip="tooltip"  data-placement="left" title="Edit data">Edit</a>

                    <button type="button" class="btn btn-danger" data-toltip="tooltip"  data-placement="right" title="Hapus data" data-toggle="modal" data-target="#myModal" id="btnHapus">Delete</button>
                </td>
              </tr>
            </tbody>
          </table>
      </div>
    </div>
    
</div>
@endsection
