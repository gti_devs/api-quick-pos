@extends('layouts.app')
@section('css')

@section('content')
<div class="container">
    @if (count($errors) > 0)
    <div class="alert alert-danger alert-dismissable">
      <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
      <h4><i class="icon fa fa-ban"></i> Gagal!</h4>
      <ul class="list-unstyled">
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
    </div>
    @endif
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{ url('/menu') }}">Table</a></li>
        <li class="breadcrumb-item active" aria-current="page">Edit</li>
      </ol>
    </nav>
    <div class="card bg-light mb-3">
      <div class="card bg-light mb-3">
        <div class="card-header float-left">
            <h4 class="float-left">Table <small> - Edit Table</small> </h4>
        </div>
        <div class="card-body">
          {!! Form::open(['method' => 'post', 'url' => '/table/'.$data->id.'/edit', 'id'=>'idForm', 'files' => true]) !!}           
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="x_panel">
                      <div class="x_content">
                          <div class="panel panel-default">
                              <div class="panel-body">
                                  <div class="row">
                                      <div class="col-md-12">
                                          <div class="form-group">
                                              {!! Form::label('no', 'Nomor Table') !!}
                                              {!! Form::text('nomor_table', isset($data)? $data->nomor_table : old('nomor_table'), array('class' => 'form-control','placeholder'=>'Nomor Table', 'data-validation' => 'required')) !!}
                                          </div>
                                      </div>
                                      <div class="col-md-12">
                                          <div class="form-group">
                                              {!! Form::label('text', 'Text') !!}
                                              {!! Form::textarea('text', isset($data)? $data->text : old('text'), array('class' => 'form-control textarea')) !!}
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="form-group">
              <a href="{{ url('/table') }}" class="btn btn-primary">Cancel</a>
              <button class="btn btn-success float-right" id="send" type="submit">
                  <i class="fa fa fa-floppy-o">
                  </i>
                  Save
              </button>
          </div>                  
          {!! Form::close() !!}
        </div>
      </div>
    </div>
</div>
@endsection
@section('js')

<script>
  
</script>
@endsection
