<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>QuickPOS</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet">

    <style type="text/css">
      .footer {
          left: 0;
          bottom: 0;
          width: 100%;
          text-align: center;
      }
    </style>
    @section ('css')
    @show
  </head>

  <body>
    <div class="navbar navbar-expand-lg fixed-top navbar-light bg-light">
      <div class="container">
        <a href="/" class="navbar-brand">Quick P.O.S</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav">
            <!-- <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" id="themes">Themes <span class="caret"></span></a>
              <div class="dropdown-menu" aria-labelledby="themes">
                <a class="dropdown-item" href="../default/">Default</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="../cerulean/">Cerulean</a>
                <a class="dropdown-item" href="../cosmo/">Cosmo</a>
              </div>
            </li> -->
            <li class="nav-item <?= (strpos(\Route::getCurrentRequest()->path(), 'menu') !== false) ? 'active' : ''; ?>">
              <a class="nav-link" href="{{ url('/menu') }}">Menu</a>
            </li>
            <li class="nav-item <?= (strpos(\Route::getCurrentRequest()->path(), 'table') !== false) ? 'active' : ''; ?>">
              <a class="nav-link" href="{{ url('/table') }}">Table</a>
            </li>
            <li class="nav-item <?= (strpos(\Route::getCurrentRequest()->path(), 'time') !== false) ? 'active' : ''; ?>">
              <a class="nav-link" href="{{ url('/time') }}">Time</a>
            </li>
            <!-- <li class="nav-item <?= (strpos(\Route::getCurrentRequest()->path(), 'payment') !== false) ? 'active' : ''; ?>">
              <a class="nav-link" href="{{ url('/payment') }}">Payment</a>
            </li> -->
            <li class="nav-item <?= (strpos(\Route::getCurrentRequest()->path(), 'user') !== false) ? 'active' : ''; ?>">
              <a class="nav-link" href="{{ url('/user') }}">User</a>
            </li>
            <!-- <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" id="download">Litera <span class="caret"></span></a>
              <div class="dropdown-menu" aria-labelledby="download">
                <a class="dropdown-item" href="https://jsfiddle.net/bootswatch/rnjfzjjo/">Open in JSFiddle</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="../4/litera/bootstrap.min.css">bootstrap.min.css</a>
                <a class="dropdown-item" href="../4/litera/bootstrap.css">bootstrap.css</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="../4/litera/_variables.scss">_variables.scss</a>
                <a class="dropdown-item" href="../4/litera/_bootswatch.scss">_bootswatch.scss</a>
              </div>
            </li> -->
          </ul>

          <ul class="nav navbar-nav ml-auto">
            @guest
                <li class="nav-item"><a class="nav-link" href="{{ route('login') }}">Login</a></li>
                <li class="nav-item"><a class="nav-link" href="{{ route('register') }}">Register</a></li>
            @else
                <li class="dropdown nav-item">
                    <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu">
                        <li class="nav-item">
                            <a class="nav-link"  href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
            @endguest
          </ul>

        </div>
      </div>
    </div>
    <br>
    <br>
    <br>
    @yield('content')
    <br>
    <br>
    <footer class="footer" style="bottom:0px;">
      <p>© 2017-2018 Company, Inc. · <a href="#">Privacy</a> · <a href="#">Terms</a></p>
    </footer>
      
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    
    <script src="{{ asset('/assets/js/vendor/popper.min.js') }}"></script>
    <script src="{{ asset('/dist/js/bootstrap.min.js') }}"></script>
    @section ('js')
    @show
  </body>
</html>
