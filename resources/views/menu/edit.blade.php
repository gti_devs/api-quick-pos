@extends('layouts.app')
@section('css')

@section('content')
<div class="container">
    @if (count($errors) > 0)
    <div class="alert alert-danger alert-dismissable">
      <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
      <h4><i class="icon fa fa-ban"></i> Gagal!</h4>
      <ul class="list-unstyled">
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
    </div>
    @endif
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{ url('/menu') }}">Menu</a></li>
        <li class="breadcrumb-item active" aria-current="page">Edit</li>
      </ol>
    </nav>
    <div class="card bg-light mb-3">
      <div class="card bg-light mb-3">
        <div class="card-header float-left">
            <h4 class="float-left">Menu <small> - Edit Menu</small> </h4>
        </div>
        <div class="card-body">
          {!! Form::open(['method' => 'post', 'url' => '/menu/'.$data->id.'/edit', 'id'=>'idForm', 'files' => true]) !!}           
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="x_panel">
                      <div class="x_content">
                          <div class="panel panel-default">
                              <div class="panel-body">
                                  <div class="row">
                                      <div class="col-md-12">
                                          <div class="form-group">
                                              {!! Form::label('images', 'Photo') !!}
                                                  {!! Form::file('photo', ['id'=>'imgUpload', 'class'=>'form-control']) !!}
                                          </div>
                                      </div>
                                      <div class="col-md-6">
                                          <div class="form-group">
                                              {!! Form::label('menu', 'Nama Menu') !!}
                                              {!! Form::text('nama_menu', isset($data)? $data->nama_menu : old('nama_menu'), array('class' => 'form-control','placeholder'=>'Nama Menu')) !!}
                                          </div>
                                      </div>
                                      <div class="col-md-6">
                                          <div class="form-group">
                                              {!! Form::label('harga', 'Harga') !!}
                                              {!! Form::text('harga', isset($data)? $data->harga : old('harga'), array('class' => 'form-control number','placeholder'=>'Harga')) !!}
                                          </div>
                                      </div>
                                      <div class="col-md-12">
                                          <div class="form-group">
                                              {!! Form::label('deskripsi', 'Description') !!}
                                              {!! Form::textarea('deskripsi', isset($data)? $data->deskripsi : old('deskripsi'), array('class' => 'form-control textarea')) !!}
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="form-group">
              <a href="{{ url('/menu') }}" class="btn btn-primary">Cancel</a>
              <button class="btn btn-success float-right" id="send" type="submit">
                  <i class="fa fa fa-floppy-o">
                  </i>
                  Save
              </button>
          </div>                  
          {!! Form::close() !!}
        </div>
      </div>
    </div>
</div>
@endsection
@section('js')
<script src="{{ asset('/assets/number_format/jquery.number.min.js')}}"></script>
<script>
  $('.number').number( true, 0, " ", "." );
</script>
@endsection
