@extends('layouts.app')

@section('css')
<link href="{{ asset('/datatable/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@section('content')

@section('content')
<div class="modal fade" id="myModal" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Hapus Data ?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <a href="#" id="Hapus" class="btn btn-danger">Delete</a>
      </div>
    </div>
  </div>
</div>
<div class="container">
    <div class="row">
      <div class="col-md-12">
        @if(Session::has('pesan'))    
        <div class="alert alert-success alert-dismissable">
          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
          <h4><i class="icon fa fa-check"></i> Sukses!</h4>
          {{ Session::get('pesan') }}
        </div>
        @endif
        @if(Session::has('error'))    
        <div class="alert alert-danger alert-dismissable">
          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
          <h4><i class="icon fa fa-times"></i> Kesalahan!</h4>
          {{ Session::get('error') }}
        </div>
        @endif
        
        @if (count($errors) > 0)
        <div class="alert alert-danger alert-dismissable">
          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
          <h4><i class="icon fa fa-ban"></i> Gagal!</h4>
          <ul class="list-unstyled">
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
        </div>
        @endif
      </div>
    </div>
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <!-- <li class="breadcrumb-item"><a href="#">Menu</a></li> -->
        <li class="breadcrumb-item active" aria-current="page">Menu</li>
      </ol>
    </nav>
    <div class="card bg-light mb-3">
      <div class="card bg-light mb-3">
        <div class="card-header float-left">
            <h4 class="float-left">Menu <small> - List Menu</small> </h4>
            <a href="{{ url('/menu/create') }}" class="btn btn-primary float-right">Create</a>
        </div>
        <div class="card-body">
          <table class="table table-hover" id="menu">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Photo</th>
                  <th scope="col">Nama Menu</th>
                  <th scope="col">Harga</th>
                  <th scope="col">Deskripsi</th>
                  <th style="width:120px">Action</th>
                </tr>
              </thead>
              <tbody>
              <?php $no = 1;?>
              @foreach($data as $d)
                <tr>
                  <th scope="row">{{ $no++ }}</th>
                  <td><img src="{{ ($d->photo) ? asset('/images/menu_photo/'.$d->photo) : asset('/images/menu_photo/t.jpg')}}" class="rounded mx-auto d-block" alt="Menu Photo" style="width:250px; height:150px; box-sizing: cover;"></td>
                  <td>{{ $d->nama_menu }}</td>
                  <td>{{ number_format($d->harga, 0, ',', '.') }}</td>
                  <td>{{ $d->deskripsi }}</td>
                  <td>
                      <a class="btn btn-warning" href="{{ url('/menu/'.$d->id.'/edit') }}" data-toltip="tooltip"  data-placement="left" title="Edit data">Edit</a>

                      <button type="button" class="btn btn-danger" data-toltip="tooltip"  data-placement="right" title="Hapus data" data-notran="{{ $d->nama_menu }}" data-idtran="{{ $d->id }}" data-toggle="modal" data-target="#myModal" id="btnHapus">Delete</button>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
        </div>
      </div>
    </div>
</div>
@endsection
@section('js')
<script src="{{ asset('/datatable/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('/datatable/dataTables.bootstrap4.min.js')}}"></script>
<script>
  $(document).ready(function() {
      $('#menu').DataTable();
  });
  $(document).ready(function() {
    $('#myModal').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget);
      var notran = button.data('notran');
      var idtran = button.data('idtran');
      var modal = $(this);
      modal.find('.modal-body').html('<p>Hapus data menu :<strong> '+notran+'</strong> ?</p>');
      modal.find('#Hapus').attr("href", "{{URL::to('menu')}}/"+idtran+"/delete");
    });
  });
</script>
@endsection
