@extends('layouts.app')

@section ('css')
<link href="{{ asset('asset/FormValidator/form-validator/theme-default.min.css')}}"
    rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    @if (count($errors) > 0)
    <div class="alert alert-danger alert-dismissable">
      <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
      <h4><i class="icon fa fa-ban"></i> Failed!</h4>
      <ul class="list-unstyled">
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
    @endif
     <section class="content-header">
      <div class="header">
        <ol class="breadcrumb">
          <li><a href="{{url('')}}"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="{{url('/user')}}">User</a></li>
          <li class="active">Create User</li>
        </ol>
      </div>
    </section>
  </div>
</div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>User <small>Create user</small> </h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        {!! Form::open(array('url' => '/user/create','id' => 'idForm')) !!}

          <div class="panel panel-default">
            <div class="panel-body">
              <div class="col-md-12">
                <div class="form-group">
                  {!!Form::label('name', 'Name')!!}
                  {!!Form::text('name','', array('class' => 'form-control','placeholder'=>'Name'))!!}
                </div>

                <div class="form-group">
                  {!!Form::label('email', 'Email')!!}
                  {!!Form::email('email','', array('class' => 'form-control','placeholder'=>'Email'))!!}
                </div> 

                <div class="form-group">
                  <label for="name">Role</label>
                  <select name="role" class="form-control">
                    <option value="">.:: Role ::.</option>
                    @foreach($role as $r)
                    <option value="{{ $r->id }}">{{ $r->display_name }}</option>
                    @endforeach
                  </select>
                </div>

              </div>
                                  
            </div>                    
          </div>

          <div class="panel panel-default">
            <div class="panel-body">
              <div class="form-group">
                {!!Form::label('password', 'Masukan Password')!!}
                {!!Form::password('password', array('class' => 'form-control','placeholder'=>'Masukan Password'))!!}
              </div>
              <div class="form-group">
                {!!Form::label('password_confirmation', 'Ulangi Password')!!}
                {!!Form::password('password_confirmation', array('class' => 'form-control','placeholder'=>'Ulangi Password'))!!}
              </div>
            </div>
          </div>          
          <div class="form-group">
              <a href="{{URL::to('/user')}}" class="btn btn-default"><i class="fa fa-arrow-left"></i> Cancel</a>
              <button id="send" type="submit" class="btn btn-success pull-right"><i class="fa fa fa-floppy-o"></i> Save </button>
          </div>
        </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>


@endsection

@section ('js')
<script src="{{ asset('asset/FormValidator/form-validator/jquery.form-validator.min.js')}}"></script>
<script type="text/javascript">
var config = {
    // ordinary validation config
    form : '#idForm',
    // reference to elements and the validation rules that should be applied
    validate : {
    'name' : {
      validation : 'required',
    },
    'email' : {
      validation : 'required, email',
    },
    'role' : {
      validation : 'required',
    },
    'password' : {
      validation : 'required, length',
      length : '5-100',
    },
    'password_confirmation' : {
      validation : 'required, confirmation',
      confirm : 'password',
    }
  }
}

$.validate({
   addValidClassOnAll : true,
   modules : 'jsconf, security',
   onModulesLoaded : function() {
    $.setupValidation(config);
   }
});

</script>
@endsection
