@extends('layouts.app')

@section('content')
<div aria-labelledby="myModalLabel" class="modal fade" id="modalhapus" role="dialog" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">
                        ×
                    </span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Hapus ?
                </h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal" type="button">
                    Batal
                </button>
                <a class="btn btn-danger" href="#" id="aHapus">
                    <strong>
                        YA
                    </strong>
                    - Hapus
                </a>
            </div>
        </div>
    </div>
</div>
<div aria-labelledby="myModalLabel" class="modal fade" id="modalReset" role="dialog" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">
                        ×
                    </span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Reset ?
                </h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal" type="button">
                    Batal
                </button>
                <a class="btn btn-success" href="#" id="aHapus">
                    <strong>
                        YA
                    </strong>
                    - Reset
                </a>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="row">
          <div class="col-md-12">
            @if(Session::has('pesan'))    
            <div class="alert alert-success alert-dismissable">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              <h4><i class="icon fa fa-check"></i> Sukses!</h4>
              {{ Session::get('pesan') }}
            </div>
            @endif
            @if(Session::has('error'))    
            <div class="alert alert-danger alert-dismissable">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              <h4><i class="icon fa fa-times"></i> Kesalahan!</h4>
              {{ Session::get('error') }}
            </div>
            @endif
            
            @if (count($errors) > 0)
            <div class="alert alert-danger alert-dismissable">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              <h4><i class="icon fa fa-ban"></i> Gagal!</h4>
              <ul class="list-unstyled">
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
            </div>
            @endif
          </div>
        </div>
        <section class="content-header">
            <div class="header">
                <ol class="breadcrumb">
                    <li>
                        <a href="{{url('/')}}">
                            <i class="fa fa-dashboard">
                            </i>
                            Home
                        </a>
                    </li>
                    <li class="active">
                        User
                    </li>
                </ol>
            </div>
        </section>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>
                    User
                    <small>
                        List user
                    </small>
                </h2>
                <a class="btn btn-round btn-info pull-right" href="{{URL::to('/user/create')}}">
                    <b>
                        <i class="fa fa-plus">
                        </i>
                        Tambah
                    </b>
                </a>
                <div class="clearfix">
                </div>
            </div>
            <div class="x_content">
                <table class="table table-responsive table-bordered table-striped data-table" id="datatable-responsive">
                    <thead>
                        <tr>
                            <th>
                                Name
                            </th>
                            <th>
                                Email
                            </th>
                            <th>
                                Roles
                            </th>
                            <th style="width:90px">
                                Action
                            </th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($hasil as $d)
                        <tr>
                            <td>
                                {{ ucwords($d->name) }}
                            </td>
                            <td>
                                {{ $d->email }}
                            </td>
                            <td>
                                {{  (empty($d->roles[0]->display_name)) ? '' : $d->roles[0]->display_name }}
                            </td>
                            <td>
                                <a class="btn btn-xs btn-warning" data-placement="left" data-toltip="tooltip" href="{{URL::to('/user/'.$d->id.'/edit')}}" title="Edit data">
                                    <span class="glyphicon glyphicon-edit">
                                    </span>
                                </a>
                                <button class="btn btn-xs btn-info" data-idtran="{{ $d->id }}" data-notran="{{ $d->name }}" data-placement="right" data-target="#modalReset" data-toggle="modal" data-toltip="tooltip" id="btnHapus" title="Password Reset" type="button">
                                    <span class="glyphicon glyphicon-refresh">
                                    </span>
                                </button>
                                <button class="btn btn-xs btn-danger" data-idtran="{{ $d->id }}" data-notran="{{ $d->name }}" data-placement="right" data-target="#modalhapus" data-toggle="modal" data-toltip="tooltip" id="btnHapus" title="Hapus data" type="button">
                                    <span class="glyphicon glyphicon-trash">
                                    </span>
                                </button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section ('js')
<script type="text/javascript">
    $(document).ready(function() {
    $('#modalhapus').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget);
      var notran = button.data('notran');
      var idtran = button.data('idtran');
      var modal = $(this);
      modal.find('.modal-body').html('<p>Yakin ingin hapus user : <strong>'+notran+'</strong> ?</p>');
      modal.find('#aHapus').attr("href", "{{url('/user/delete')}}/"+idtran);
    });
  });

  $(document).ready(function() {
    $('#modalReset').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget);
      var notran = button.data('notran');
      var idtran = button.data('idtran');
      var modal = $(this);
      modal.find('.modal-body').html('<p>Yakin akan me-reset password user : <strong>'+notran+'</strong> ?</p>');
      modal.find('#aHapus').attr("href", "{{url('/user')}}/"+idtran+'/reset');
    });
  });
</script>
@endsection
