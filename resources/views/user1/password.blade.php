@extends('layouts.app')
@section ('css')
<link href="{{ asset('assets/FormValidator/form-validator/theme-default.min.css')}}"
    rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    @if (count($errors) > 0)
    <div class="alert alert-danger alert-dismissable">
      <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
      <h4><i class="icon fa fa-ban"></i> Failed!</h4>
      <ul class="list-unstyled">
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
    @endif
     <section class="content-header">
      <div class="header">
        <ol class="breadcrumb">
          <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Change Password</li>
        </ol>
      </div>
    </section>
  </div>
</div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Change Password <small></small> </h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="col-md-12">
        <div class="center-block">
          {!! Form::open(array('url' => '/password', 'id' => 'idForm')) !!}

          <div class="panel panel-default">
            <div class="panel-body">
              <div class="form-group">
                <label>Password Lama<span class="text-danger"> *</span></label>
                {!!Form::password('old_password', array('class' => 'form-control','placeholder'=>'Masukan Password Lama'))!!}
              </div>
              <div class="form-group">
                <label>Password Baru<span class="text-danger"> *</span></label>
                {!!Form::password('password', array('class' => 'form-control','placeholder'=>'Password Baru'))!!}
              </div>
              <div class="form-group">
                <label>Ulangi Password Baru<span class="text-danger"> *</span></label>
                {!!Form::password('password_confirmation', array('class' => 'form-control','placeholder'=>'Ulangi Password Baru'))!!}
              </div>
              <div class="form-group">
                  <button type="submit" class="btn btn-success pull-right"><span class="glyphicon glyphicon-save"></span> Save </button>
                  <a href="{{URL::to('/')}}" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left"></span> Kembali</a>
              </div>
            </div>
          </div>
          {!! Form::close() !!}
        </div>
      </div>
      </div>
    </div>
  </div>
</div>


@endsection

@section ('js')
<script src="{{ asset('assets/FormValidator/form-validator/jquery.form-validator.min.js')}}"></script>
<script type="text/javascript">
var config = {
    // ordinary validation config
    form : '#idForm',
    // reference to elements and the validation rules that should be applied
    validate : {
    'old_password' : {
      validation : 'required',
    },
    'password' : {
      validation : 'required, length',
      length : '5-12',
    },
    'password_confirmation' : {
      validation : 'required, confirmation',
      confirm : 'password',
    }
  }
}

$.validate({
   addValidClassOnAll : true,
   modules : 'jsconf, security',
   onModulesLoaded : function() {
    $.setupValidation(config);
   }
});

</script>
@endsection