@extends('layouts.app')
@section ('css')
<link href="{{ asset('assets/FormValidator/form-validator/theme-default.min.css')}}"
    rel="stylesheet" type="text/css" />
<style type="text/css">
  #image-preview {
    width: 200px;
    height: 200px;
    position: relative;
    overflow: hidden;
    background-color: #ffffff;
    color: #ecf0f1;
  }
  #image-preview input {
    line-height: 200px;
    font-size: 200px;
    position: absolute;
    opacity: 0;
    z-index: 10;
  }
  #image-preview label {
    position: absolute;
    z-index: 5;
    opacity: 0.8;
    cursor: pointer;
    background-color: #bdc3c7;
    width: 100px;
    height: 50px;
    font-size: 12px;
    line-height: 50px;
    text-transform: uppercase;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    margin: auto;
    text-align: center;
  }
</style>
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    @if (count($errors) > 0)
    <div class="alert alert-danger alert-dismissable">
      <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
      <h4><i class="icon fa fa-ban"></i> Failed!</h4>
      <ul class="list-unstyled">
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
    @endif
     <section class="content-header">
      <div class="header">
        <ol class="breadcrumb">
          <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Change Profile</li>
        </ol>
      </div>
    </section>
  </div>
</div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Change Profile <small></small> </h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="center-block">
          {!! Form::open(array('url' => '/profile', 'id' => 'idform', 'files'=>'true')) !!}

            <div class="panel panel-default">
              <div class="panel-body">

                <div class="col-md-3">
                  <div id="image-preview" style="background: url('{{ asset('avatar/'.$user->foto) }}');background-size: cover;background-position: center center;" class="img-circle">
                    <label for="image-upload" id="image-label">Choose File</label>
                    <input type="file" name="image" id="image-upload" />
                  </div>
                </div>
                <div class="col-md-9">
                  <div class="form-group">
                    <label>Nama<span class="text-danger"> *</span></label>
                     {!! Form::text('name', $user->name, ['class' => 'form-control', 'required' => 'required']) !!}
                  </div>
                  <div class="form-group">
                    <label>Email<span class="text-danger"> *</span></label>
                     {!! Form::text('email', $user->email, ['class' => 'form-control', 'required' => 'required']) !!}
                  </div>
                  <div class="form-group">
                      <button type="submit" class="btn btn-success pull-right"><span class="glyphicon glyphicon-save"></span> Save </button>
                      <a href="{{URL::to('/')}}" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left"></span> Kembali</a>
                  </div>
                </div>
              </div>
            </div>
            {!! Form::close() !!}
        </div>
      </div>
      </div>
    </div>
  </div>
</div>


@endsection

@section ('js')
<script src="{{ asset('asset/build/js/jquery.upload_preview.min.js')}}"></script>
<script src="{{ asset('asset/FormValidator/form-validator/jquery.form-validator.min.js')}}"></script>
<script type="text/javascript">
var config = {
    // ordinary validation config
    form : '#idForm',
    // reference to elements and the validation rules that should be applied
    validate : {
    'name' : {
      validation : 'required',
    },
    'email' : {
      validation : 'required, email',
    }
  }
}

$.validate({
   addValidClassOnAll : true,
   modules : 'jsconf, security',
   onModulesLoaded : function() {
    $.setupValidation(config);
   }
});

$(document).ready(function() {
  $.uploadPreview({
    input_field: "#image-upload",
    preview_box: "#image-preview",
    label_field: "#image-label"
  });
});
</script>
@endsection