@extends('layouts.app')

@section('css')
<link href="{{ asset('/datatable/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@section('content')

@section('content')
<div class="modal fade" id="modalhapus" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Hapus Data ?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <a href="#" id="Hapus" class="btn btn-danger">Delete</a>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalReset" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Reset Password ?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <a href="#" id="Reset" class="btn btn-info">Reset</a>
      </div>
    </div>
  </div>
</div>
<div class="container">
    <div class="row">
      <div class="col-md-12">
        @if(Session::has('pesan'))    
        <div class="alert alert-success alert-dismissable">
          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
          <h4><i class="icon fa fa-check"></i> Sukses!</h4>
          {{ Session::get('pesan') }}
        </div>
        @endif
        @if(Session::has('error'))    
        <div class="alert alert-danger alert-dismissable">
          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
          <h4><i class="icon fa fa-times"></i> Kesalahan!</h4>
          {{ Session::get('error') }}
        </div>
        @endif
        
        @if (count($errors) > 0)
        <div class="alert alert-danger alert-dismissable">
          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
          <h4><i class="icon fa fa-ban"></i> Gagal!</h4>
          <ul class="list-unstyled">
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
        </div>
        @endif
      </div>
    </div>
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <!-- <li class="breadcrumb-item"><a href="#">Menu</a></li> -->
        <li class="breadcrumb-item active" aria-current="page">User</li>
      </ol>
    </nav>
    <div class="card bg-light mb-3">
      <div class="card bg-light mb-3">
        <div class="card-header float-left">
            <h4 class="float-left">User <small> - List User</small> </h4>
            <a href="{{ url('/user/create') }}" class="btn btn-primary float-right">Create</a>
        </div>
        <div class="card-body">
          <table class="table table-hover" id="user">
              <thead>
                  <tr>
                      <th>
                          Name
                      </th>
                      <th>
                          Email
                      </th>
                      <th >
                          Action
                      </th>
                  </tr>
              </thead>
              <tbody>
              <?php $no = 1;?>
              @foreach($hasil as $d)
                <tr>
                    <td>
                        {{ ucwords($d->name) }}
                    </td>
                    <td>
                        {{ $d->email }}
                    </td>
                    <td style="width:280px">
                        <a class="btn btn-warning" data-placement="left" data-toltip="tooltip" href="{{URL::to('/user/'.$d->id.'/edit')}}" title="Edit data"> Edit </a>

                        <button class="btn btn-info" data-idtran="{{ $d->id }}" data-notran="{{ $d->name }}" data-placement="right" data-target="#modalReset" data-toggle="modal" data-toltip="tooltip" id="btnHapus" title="Password Reset" type="button">
                            Reset Password
                        </button>
                        <button class="btn btn-xs btn-danger" data-idtran="{{ $d->id }}" data-notran="{{ $d->name }}" data-placement="right" data-target="#modalhapus" data-toggle="modal" data-toltip="tooltip" id="btnHapus" title="Hapus data" type="button">
                            Delete
                        </button>
                    </td>
                </tr>
                @endforeach
              </tbody>
            </table>
        </div>
      </div>
    </div>
</div>
@endsection
@section('js')
<script src="{{ asset('/datatable/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('/datatable/dataTables.bootstrap4.min.js')}}"></script>
<script>
  $(document).ready(function() {
      $('#user').DataTable();
  });
  $(document).ready(function() {
    $('#modalhapus').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget);
      var notran = button.data('notran');
      var idtran = button.data('idtran');
      var modal = $(this);
      modal.find('.modal-body').html('<p>Yakin ingin hapus user : <strong>'+notran+'</strong> ?</p>');
      modal.find('#Hapus').attr("href", "{{url('/user/delete')}}/"+idtran);
    });
  });

  $(document).ready(function() {
    $('#modalReset').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget);
      var notran = button.data('notran');
      var idtran = button.data('idtran');
      var modal = $(this);
      modal.find('.modal-body').html('<p>Yakin akan me-reset password user : <strong>'+notran+'</strong> ?</p>');
      modal.find('#Reset').attr("href", "{{url('/user')}}/"+idtran+'/reset');
    });
  });
</script>
@endsection
