<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/




Route::get('/menu', 'Api\MenuController@index');
Route::get('/menu/{id}', 'MenuController@show');
Route::post('/menu', 'MenuController@store');
Route::put('/menu', 'MenuController@update');
Route::delete('/menu/{id}', 'MenuController@destroy');

Route::group(['middleware' => 'auth:api'], function(){
	
	

});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
