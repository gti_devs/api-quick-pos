<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/menu', 'MenuController@index');
Route::get('/menu/create', 'MenuController@create');
Route::post('/menu/create', 'MenuController@store');
Route::get('/menu/{id}/edit', 'MenuController@edit');
Route::post('/menu/{id}/edit', 'MenuController@update');
Route::get('/menu/{id}/delete', 'MenuController@destroy');


Route::get('/table', 'TableController@index');
Route::get('/table/create', 'TableController@create');
Route::post('/table/create', 'TableController@store');
Route::get('/table/{id}/edit', 'TableController@edit');
Route::post('/table/{id}/edit', 'TableController@update');
Route::get('/table/{id}/delete', 'TableController@destroy');


Route::get('/time', 'TimeController@index');
Route::get('/time/create', 'TimeController@create');
Route::post('/time/create', 'TimeController@store');
Route::get('/time/{id}/edit', 'TimeController@edit');
Route::post('/time/{id}/edit', 'TimeController@update');
Route::get('/time/{id}/delete', 'TimeController@destroy');

Route::get('user', 'UserController@index');
Route::get('user/create', 'UserController@create');
Route::post('user/create', 'UserController@store');
Route::get('user/{id}/edit', 'UserController@edit');
Route::post('user/{id}/edit', 'UserController@update');
Route::get('user/delete/{id}', 'UserController@destroy');
Route::get('user/{id}/reset', 'UserController@reset');

Route::get('/home', 'HomeController@index');

Route::group(['middleware' => ['web']], function () {
    
});

