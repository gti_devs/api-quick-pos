<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Table;

class TableController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $data = Table::all();
        return view('table.index', compact('data'));
    }


    public function create()
    {
        return view('table.create');
    }


    public function store(Request $r)
    {
        
        $this->validate($r, [
            'nomor_table' => 'required|numeric', 
        ]);

        $data = new Table;
        $data->nomor_table    = $r->nomor_table;
        $data->text           = $r->text;

        if($data->save()){
            return redirect('table')
                            ->with('pesan','Data Berhasil di Input!');
        }
    }

    public function edit($id)
    {
        $data = Table::findOrFail($id);
        return view('table.edit', compact('data'));
    }

    public function update(Request $r, $id)
    {
        $this->validate($r, [
            'nomor_table'     => 'required|numeric',
        ]);

        $data = Table::findOrFail($id);
        $data->nomor_table    = $r->nomor_table;
        $data->text           = $r->text;

        if($data->save()){
            return redirect('table')
                            ->with('pesan','Data Berhasil di Update!');
        }
    }

    public function destroy($id)
    {
        $data = Table::findOrFail($id);
        
        if($data->delete()){
            return redirect('table')
                            ->with('pesan','Data Berhasil di Hapus!');
        }
    }
}
