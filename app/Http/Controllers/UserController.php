<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use DB;
use App\User;
use App\Role;
use App\Permission;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        
        $hasil = User::all();
        
        return view('user.index', compact('hasil'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create', compact('role'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        $this->validate($r, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'password_confirmation' => 'required|min:6'

        ]);

        $password = $r->password;
        $password = bcrypt($password);

        $user = new User;
        $user->email = $r->email;
        $user->name = $r->name;
        $user->password = $password;
        $user->save();

        return redirect('/user')
            ->with('pesan','User telah di tambah !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = User::findOrFail($id);
        return view('user.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {
        $this->validate($r, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255'
        ]);

        $user = User::findOrFail($id);
        $user->email = $r->email;
        $user->name = $r->name;
        $user->save();

        return redirect('/user')
            ->with('pesan','User telah di update!');
    }


    public function reset($id)
    {
        $password = 'default1234';
        $password = bcrypt($password);

        $user = User::findOrFail($id);
        $user->password = $password;
        $user->save();

        return redirect('/user')
            ->with('pesan','Password user ('.$user->name.') telah di reset, menjadi "default1234" (tanpa tanda petik)');
    }

    public function active($id)
    {

        $user = User::find($id);
        $user->active = 1;
        $user->save();

        return redirect('/user')
            ->with('pesan','Account user telah di aktif kan !');
    }

    public function unactive($id)
    {

        $user = User::find($id);
        $user->active = 0;
        $user->save();

        return redirect('/user')
            ->with('pesan','Account user telah di un-aktif kan !');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        $user = User::destroy($id);

        if($user)
            Session::flash('pesan', 'User telah di hapus!');
        return redirect('/user');
    }
}
