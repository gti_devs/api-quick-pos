<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Time;

class TimeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $data = Time::all();
        return view('time.index', compact('data'));
    }


    public function create()
    {
        return view('time.create');
    }


    public function store(Request $r)
    {
        
        $this->validate($r, [
            'time' => 'required', 
        ]);

        $data = new Time;
        $data->time    = $r->time;
        $data->text    = $r->text;

        if($data->save()){
            return redirect('time')
                            ->with('pesan','Data Berhasil di Input!');
        }
    }

    public function edit($id)
    {
        $data = Time::findOrFail($id);
        return view('time.edit', compact('data'));
    }

    public function update(Request $r, $id)
    {
        $this->validate($r, [
            'time' => 'required', 
        ]);

        $data = Time::findOrFail($id);
        $data->time    = $r->time;
        $data->text    = $r->text;

        if($data->save()){
            return redirect('time')
                            ->with('pesan','Data Berhasil di Update!');
        }
    }

    public function destroy($id)
    {
        $data = Time::findOrFail($id);
        
        if($data->delete()){
            return redirect('time')
                            ->with('pesan','Data Berhasil di Hapus!');
        }
    }
}
