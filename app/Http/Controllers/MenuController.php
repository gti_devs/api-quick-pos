<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Menu;
use File;

class MenuController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $data = Menu::all();
        return view('menu.index', compact('data'));
    }


    public function create()
    {
        return view('menu.create');
    }


    public function show($id)
    {
        $data = Menu::findOrFail($id);
        return view('menu.index');
    }



    public function store(Request $r)
    {
        
        $this->validate($r, [
            'nama_menu' => 'required', 
            'harga'     => 'required|numeric',
            'photo'     => 'mimes:jpg,jpeg,png',
        ]);

        if ($r->hasFile('photo'))
        {
            $file_path = public_path().'/images/menu_photo';
            if(!File::exists($file_path)) {
                File::makeDirectory($file_path, 0777, true);
            }        
            $photoname  = time().'.'.$r->photo->getClientOriginalExtension();
            $r->photo->move(public_path('/images/menu_photo'), $photoname);
        }

        $data = new Menu;
        $data->nama_menu    = $r->nama_menu;
        $data->harga        = $r->harga;
        $data->deskripsi    = $r->deskripsi;
        $data->photo        = $photoname;

        if($data->save()){
            return redirect('menu')
                            ->with('pesan','Data Berhasil di Input!');
        }
    }

    public function edit($id)
    {
        $data = Menu::findOrFail($id);
        return view('menu.edit', compact('data'));
    }

    public function update(Request $r, $id)
    {
        $this->validate($r, [
            'nama_menu' => 'required', 
            'harga'     => 'required|numeric',
            // 'photo'     => 'mimes:jpg,png',
        ]);

        $data = Menu::findOrFail($id);
        $data->nama_menu    = $r->nama_menu;
        $data->harga        = $r->harga;
        $data->deskripsi    = "";
        $data->photo        = "-";

        if($data->save()){
            return redirect('menu')
                            ->with('pesan','Data Berhasil di Update!');
        }
    }

    public function destroy($id)
    {
        $data = Menu::findOrFail($id);
        $file_path  = public_path().'/images/menu_photo/'.$data->photo;
        if($data){
            if(File::exists($file_path)){
                File::delete($file_path);
            }
        }
        if($data->delete()){
            return redirect('menu')
                            ->with('pesan','Data Berhasil di Hapus!');
        }
    }
}
