<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Menu;
use App\Http\Resources\MenuResource;

class MenuApiController extends Controller
{


    public function index()
    {
        $data = Menu::paginate(10);
        return MenuResource::collection($data);
    }



    public function show($id)
    {
        $data = Menu::findOrFail($id);
        return new MenuResource($data);
    }



    public function store(Request $r)
    {

    	$data = new Menu;
    	$data->nama_menu 	= $r->nama_menu;
    	$data->harga 		= $r->harga;
    	$data->photo 		= $r->photo;

    	if($data->save()){
        	return new MenuResource($data);
    	}
    }

    public function update(Request $r)
    {

    	$data = Menu::findOrFail($r->menu_id);
    	$data->nama_menu 	= $r->nama_menu;
    	$data->harga 		= $r->harga;
    	$data->photo 		= $r->photo;

    	if($data->save()){
        	return new MenuResource($data);
    	}
    }

    public function destroy($id)
    {
        $data = Menu::findOrFail($id);

        if($data->delete()){
        	return new MenuResource($data);
        }
    }
}
