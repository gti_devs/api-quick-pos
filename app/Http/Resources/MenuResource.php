<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class MenuResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id'        => $this->id,
            'nama_menu' => $this->nama_menu,
            'harga'     => $this->harga,
            'photo'     => $this->photo,
        ];
    }


    public function with($request)
    {
        return [
            'version' => '0.0.1',
            'author_url' => 'http://gtionline.id/id', 
        ];
    }
}
